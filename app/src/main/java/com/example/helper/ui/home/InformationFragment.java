package com.example.helper.ui.home;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.ui.DialogAddParts;
import com.example.helper.R;
import com.example.helper.databinding.FragmentInformationBinding;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;
import com.example.helper.viewmodels.InformationViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class InformationFragment extends Fragment{
    public FragmentInformationBinding binding;
    public InformationViewModel viewModel;
    public DatabaseReference mDataBase;
    public FirebaseAuth mAuth;
    public String user;
    public Dialog dialog;
    public int i, key;
    public boolean b;
    public View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentInformationBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        viewModel = new ViewModelProvider(this).get(InformationViewModel.class);
        SaveID saveID = new SaveID();

        binding.addParts.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            user = FirebaseAuth.getInstance().getCurrentUser().getUid();
            binding.addParts.setVisibility(View.VISIBLE);
            ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
            ((MainActivity) requireActivity()).like.setVisibility(View.VISIBLE);
        }

        if (user != null){
            viewModel.DataBool(saveID.getTitle(), user);

            mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
            mDataBase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    i = 0;
                    key = 0;
                    for (DataSnapshot ds: snapshot.getChildren()){
                        key = Integer.parseInt(ds.getKey());
                        String ids = ds.child("title").getValue(String.class);
                        if (ids.equals(saveID.getTitle())){
                            i = Integer.parseInt(ds.getKey());
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });
        }

        viewModel.infoData(saveID.getID(), saveID.getName());

        LiveData<Boolean> data = viewModel.data;
        data.observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean bool) {
                if (bool){
                    b = bool;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                }
            }
        });

        LiveData<String> Data = viewModel.Data;
        Data.observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.text.setText(saveID.getTitle());
                Picasso.get().load(s).into(binding.image);
            }
        });

        ((MainActivity) requireActivity()).like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!b){
                    b = true;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                    viewModel.Like(key, user);
                }else {
                    b = false;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
                    viewModel.removeLike(i, user);
                }
            }
        });

        binding.addParts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAddParts add = new DialogAddParts(getActivity());
                add.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                add.show();
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) requireActivity()).like.setVisibility(View.GONE);
    }
}