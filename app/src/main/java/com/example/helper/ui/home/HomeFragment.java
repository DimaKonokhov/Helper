package com.example.helper.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.helper.R;
import com.example.helper.databinding.FragmentHomeBinding;
import com.example.helper.ui.List.RecyclerAdapter;

public class HomeFragment extends Fragment {
    private FragmentHomeBinding binding;
    private RecyclerAdapter recyclerAdapter;
    private String[] list = {"Процессоры", "Материнские платы", "Видеокарты", "Оперативная память", "Блоки питания", "Накопители SSD", "Охлаждение процессора", "Корпуса", "Охлаждение корпуса"};
    private int[] DrawableList = {R.drawable.processor, R.drawable.motherboard, R.drawable.video_card, R.drawable.ram, R.drawable.power_supply_unit, R.drawable.ssd_drive, R.drawable.processor_cooler, R.drawable.computer_case, R.drawable.cooling_of_the_computer_case};
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        
        binding.recycler.setHasFixedSize(true);

        binding.recycler.setHasFixedSize(true);

        recyclerAdapter = new RecyclerAdapter(list, DrawableList, getActivity());
        binding.recycler.setAdapter(recyclerAdapter);
        return view;
    }
}