package com.example.helper.ui.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;

import java.util.ArrayList;
import java.util.List;

public class AdapterArticles  extends RecyclerView.Adapter<AdapterArticles.RecyclerviewHolder> {
    private final int numberItems;
    public List<String> LT = new ArrayList<>();
    private Context fragment;

    public AdapterArticles(List<String> list, Context fragment) {
        numberItems = list.size();
        LT = list;
        this.fragment = fragment;
    }


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public AdapterArticles.RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.articles;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        RecyclerviewHolder viewHolder = new RecyclerviewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewHolder holder, int viewHolderCount) {
        holder.bind(viewHolderCount);
    }


    @Override
    public int getItemCount() {
        return numberItems;
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {

        TextView ItemList;

        public RecyclerviewHolder(@NonNull View itemView) {
            super(itemView);

            ItemList = itemView.findViewById(R.id.titles);
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SaveID saveID = new SaveID();
                    saveID.setName((String) ItemList.getText());
                    if (Navigation.findNavController(view).getCurrentDestination().getId() == R.id.articlesFragment){
                        Navigation.findNavController(view).navigate(R.id.articlesInfoFragment);
                    }else {
                        Navigation.findNavController(view).navigate(R.id.favoritesArticleStateFragment);
                    }
                }
            });

        }

        void bind(int ListIndex) {
            ItemList.setText(LT.get(ListIndex));
        }

    }
}
