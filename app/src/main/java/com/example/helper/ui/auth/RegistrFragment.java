package com.example.helper.ui.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.helper.R;
import com.example.helper.databinding.FragmentRegistrBinding;
import com.example.helper.ui.MainActivity;
import com.example.helper.viewmodels.RegistrViewModel;

public class RegistrFragment extends Fragment {
    private FragmentRegistrBinding binding;
    private RegistrViewModel mViewModel;
    private View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentRegistrBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        binding.registration.setEnabled(false);

        mViewModel = new ViewModelProvider(this).get(RegistrViewModel.class);

        EditText[] edList = {binding.login, binding.password,binding.passwordRepeat, binding.name};
        AuthFragment.CustomTextWatcher textWatcher = new AuthFragment.CustomTextWatcher(edList, binding.registration);
        for (EditText editText : edList) editText.addTextChangedListener(textWatcher);

        LiveData<Boolean> data = mViewModel.data;
        data.observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean b) {
                binding.registration.setEnabled(b);
            }
        });

        binding.registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.password.getText().toString().equals(binding.passwordRepeat.getText().toString())) {
                    if (binding.password.getText().toString().length() >= 6) {
                        mViewModel.sentData(binding.login.getText().toString(), binding.passwordRepeat.getText().toString(), binding.name.getText().toString());
                        Navigation.findNavController(view).navigate(R.id.action_registrFragment_to_homeFragment);
                    } else {
                        binding.error.setText("Пароль содержит меньше 6 символов");
                        binding.error.setVisibility(View.VISIBLE);
                    }
                } else {
                    binding.error.setText("Вы не подтвердили пароль!");
                    binding.error.setVisibility(View.VISIBLE);
                }
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
        super.onStart();
    }
    @Override
    public void onDestroy() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        super.onDestroy();
    }
}