package com.example.helper.ui.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.databinding.FragmentArticlesBinding;
import com.example.helper.ui.List.AdapterArticles;
import com.example.helper.viewmodels.ArticlesViewModel;

import java.util.List;

public class ArticlesFragment extends Fragment {
    private ArticlesViewModel articlesViewModel;
    private FragmentArticlesBinding binding;
    private AdapterArticles recyclerAdapter;
    private View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentArticlesBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        articlesViewModel = new ViewModelProvider(this).get(ArticlesViewModel.class);
        articlesViewModel.Data();

        LiveData<List<String>> data = articlesViewModel.liveData;
        data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> list) {
                binding.recyclerArticles.setHasFixedSize(true);
                recyclerAdapter = new AdapterArticles(list, getActivity());
                binding.recyclerArticles.setAdapter(recyclerAdapter);
            }
        });
        return view;
    }
}