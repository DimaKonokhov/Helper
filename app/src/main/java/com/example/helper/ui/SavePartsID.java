package com.example.helper.ui;

import java.util.List;

public class SavePartsID {
    private static List<String> LS = null;
    private static String title;
    private static String id;
    public void setLS(List<String> LS){
        this.LS = LS;
    }
    public List<String> getLS(){
        return LS;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return title;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return id;
    }
}
