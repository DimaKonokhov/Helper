package com.example.helper.ui.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.SavePartsID;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class AdapterParts extends RecyclerView.Adapter<AdapterParts.RecyclerviewHolder> {
    private final int numberItems;
    public List<String> LT = new ArrayList<>();
    public Context fragment;
    private DatabaseReference mDataBase;

    public AdapterParts(List<String> list, Context fragment) {
        numberItems = list.size();
        LT = list;
        this.fragment = fragment;
    }


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public AdapterParts.RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.my_parts;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        AdapterParts.RecyclerviewHolder viewHolder = new AdapterParts.RecyclerviewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterParts.RecyclerviewHolder holder, int viewHolderCount) {
        holder.bind(viewHolderCount);
    }


    @Override
    public int getItemCount() {
        return numberItems;
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {

        TextView ItemList;

        public RecyclerviewHolder(@NonNull View itemView) {
            super(itemView);

            ItemList = itemView.findViewById(R.id.title_parts);
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SavePartsID savePartsID = new SavePartsID();
                    savePartsID.setTitle((String) ItemList.getText());
                    Navigation.findNavController(view).navigate(R.id.myPartsFragment);
                }
            });

        }

        void bind(int ListIndex) {
            ItemList.setText(LT.get(ListIndex));
        }

    }
}
