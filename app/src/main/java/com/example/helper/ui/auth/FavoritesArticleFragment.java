package com.example.helper.ui.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.databinding.FragmentFavoritesArticleBinding;
import com.example.helper.ui.List.AdapterArticles;
import com.example.helper.viewmodels.FavoritesArticleViewModel;

import java.util.List;

public class FavoritesArticleFragment extends Fragment {
    private FragmentFavoritesArticleBinding binding;
    public FavoritesArticleViewModel viewModel;
    public AdapterArticles adapter;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentFavoritesArticleBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        viewModel = new ViewModelProvider(this).get(FavoritesArticleViewModel.class);
        viewModel.Data();

        LiveData<List<String>> data = viewModel.data;
        data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> list) {
                binding.recyclerArticles.setHasFixedSize(true);
                adapter = new AdapterArticles(list, getActivity());
                binding.recyclerArticles.setAdapter(adapter);
            }
        });
        return view;
    }
}