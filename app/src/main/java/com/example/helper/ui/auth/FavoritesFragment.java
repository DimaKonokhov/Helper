package com.example.helper.ui.auth;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.helper.R;
import com.example.helper.databinding.FragmentFavoritesBinding;
import com.example.helper.ui.MainActivity;

public class FavoritesFragment extends Fragment {
    private FragmentFavoritesBinding binding;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.articles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.favoritesArticleFragment);
            }
        });

        binding.parts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.favoritesPartsFragment);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
        super.onStart();
    }

    @Override
    public void onDestroy() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        super.onDestroy();
    }
}