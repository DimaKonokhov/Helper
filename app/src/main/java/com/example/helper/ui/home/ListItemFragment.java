package com.example.helper.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.databinding.FragmentListItemBinding;
import com.example.helper.ui.List.AdapterFragmentList;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;
import com.example.helper.viewmodels.ListItemViewModel;

import java.util.List;

public class ListItemFragment extends Fragment {
    private ListItemViewModel mViewModel;
    private FragmentListItemBinding binding;
    private AdapterFragmentList adapterFragmentList;
    private String[] list = {};
    private String[] DrawableList = {};
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentListItemBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        mViewModel = new ViewModelProvider(this).get(ListItemViewModel.class);
        SaveID saveID = new SaveID();
        mViewModel.Data(saveID.getName());

        LiveData<List<String>> Data = mViewModel.Data;
        Data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> list) {
                DrawableList = list.toArray(new String[list.size()]);
            }
        });

        LiveData<List<String>> data = mViewModel.data;
        data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> s) {
                list = s.toArray(new String[s.size()]);
                binding.recycler.setHasFixedSize(true);
                adapterFragmentList = new AdapterFragmentList(list, DrawableList, getActivity());
                binding.recycler.setAdapter(adapterFragmentList);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
        super.onStart();
    }
    @Override
    public void onDestroy() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        super.onDestroy();
    }
}