package com.example.helper.ui.auth;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.helper.R;
import com.example.helper.databinding.FragmentMyPartsStateBinding;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;
import com.example.helper.ui.SavePartsID;
import com.example.helper.viewmodels.MyPartsStateViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class MyPartsStateFragment extends Fragment {

    private MyPartsStateViewModel mViewModel;
    private FragmentMyPartsStateBinding binding;
    private DatabaseReference mDataBase;
    public int i ,key, count;
    public boolean b;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentMyPartsStateBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        mViewModel = new ViewModelProvider(this).get(MyPartsStateViewModel.class);
        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        SaveID saveID = new SaveID();
        SavePartsID savePartsID = new SavePartsID();

        mViewModel.mData(user, saveID.getTitle(), savePartsID.getTitle());
        mViewModel.BoolLike(user, saveID.getTitle());

        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                i = 0;
                key = 0;
                for (DataSnapshot ds: snapshot.getChildren()){
                    key = Integer.parseInt(ds.getKey());
                    String ids = ds.child("title").getValue(String.class);
                    if (ids.equals(saveID.getTitle())){
                        i = Integer.parseInt(ds.getKey());
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });

        LiveData<String> data = mViewModel.data;
        data.observe(requireActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.text.setText(saveID.getTitle());
                Picasso.get().load(s).into(binding.image);
            }
        });

        LiveData<Boolean> Data = mViewModel.Data;
        Data.observe(requireActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (count == 0){
                    count++;
                    b = true;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                }
            }
        });

        ((MainActivity) requireActivity()).like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!b){
                    b = true;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                    mViewModel.Like(key, user);
                }else {
                    b = false;
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
                    mViewModel.removeLike(i, user);
                }
            }
        });

        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.removeMyParts(savePartsID.getId(), user, savePartsID.getTitle());
                ((MainActivity) getActivity()).onSupportNavigateUp();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        b = false;
        ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity) requireActivity()).like.setVisibility(View.VISIBLE);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) requireActivity()).like.setVisibility(View.GONE);
    }
}