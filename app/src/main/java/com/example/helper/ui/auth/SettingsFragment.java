package com.example.helper.ui.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.helper.R;
import com.example.helper.databinding.FragmentSettingsBinding;
import com.example.helper.ui.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SettingsFragment extends Fragment {
   private FragmentSettingsBinding binding;
   public View view;
   public int count = 0;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        binding.passwordChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.changePasswordFragment);
            }
        });

        binding.exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Navigation.findNavController(view).navigate(R.id.action_settingsFragment_to_homeFragment);
            }
        });

        binding.errors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                if (count == 3) {
                    Navigation.findNavController(view).navigate(R.id.senpaiRizedFragment);
                }
            }
        });
        return view;
    }

    private void ProfileValue(String Key, Integer id){
        String user;
        user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users").child(user).child(Key);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                TextView text = view.findViewById(id);
                assert text != null;
                text.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError error) {}
        });
    }

    @Override
    public void onStart() {
        count = 0;
        ProfileValue("login", R.id.name);
        ProfileValue("email", R.id.post);
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
        super.onStart();
    }
    @Override
    public void onDestroy() {
        count = 0;
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        super.onDestroy();
    }
}