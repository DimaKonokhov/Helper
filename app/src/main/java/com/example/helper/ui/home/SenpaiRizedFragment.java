package com.example.helper.ui.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.helper.databinding.FragmentSenpaiRizedBinding;
import com.example.helper.ui.MainActivity;

public class SenpaiRizedFragment extends Fragment {
    private FragmentSenpaiRizedBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSenpaiRizedBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.senpai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/Senpai0210"));
                startActivity(browserIntent);
            }
        });

        binding.rized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/RizedSW"));
                startActivity(browserIntent);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        super.onStart();
    }
}