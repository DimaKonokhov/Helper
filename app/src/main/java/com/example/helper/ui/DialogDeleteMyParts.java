package com.example.helper.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.helper.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DialogDeleteMyParts extends Dialog {
    public Activity activity;
    public DatabaseReference mDataBase;
    public String user;
    public String title;
    public TextView no;
    public TextView delete;
    public NavController navController;
    public DialogDeleteMyParts(Activity a, String user, String title) {
        super(a);
        activity = a;
        this.user = user;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_delete_my_parts);

        no = findViewById(R.id.no);
        delete = findViewById(R.id.delete_my_parts);
        navController = Navigation.findNavController(activity, R.id.navHostFragmentActivityMain);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts").child(title);
                mDataBase.removeValue();
                navController.navigateUp();
                dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
