package com.example.helper.ui.auth;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.helper.R;
import com.example.helper.databinding.FragmentFavoritesPartsBinding;
import com.example.helper.ui.List.AdapterLikeParts;
import com.example.helper.ui.List.RecyclerAdapter;
import com.example.helper.viewmodels.FavoritesPartsViewModel;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class FavoritesPartsFragment extends Fragment {
    private FavoritesPartsViewModel mViewModel;
    private FragmentFavoritesPartsBinding binding;
    private AdapterLikeParts adapter;
    private String[] list = {};
    private String[] DrawableList = {};

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentFavoritesPartsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        mViewModel = new ViewModelProvider(this).get(FavoritesPartsViewModel.class);
        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mViewModel.mData(user);

        LiveData<List<String>> data = mViewModel.data;
        data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                DrawableList = strings.toArray(new String[strings.size()]);
            }
        });

        LiveData<List<String>> Data = mViewModel.Data;
        Data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                list = strings.toArray(new String[strings.size()]);
                binding.recycler.setHasFixedSize(true);
                adapter = new AdapterLikeParts(list, DrawableList, getActivity());
                binding.recycler.setAdapter(adapter);
            }
        });
        return view;
    }
}