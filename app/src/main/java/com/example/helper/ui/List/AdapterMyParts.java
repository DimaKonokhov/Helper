package com.example.helper.ui.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.SaveID;
import com.example.helper.ui.SavePartsID;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AdapterMyParts extends RecyclerView.Adapter<AdapterMyParts.RecyclerviewHolder> {
    private final int numberItems;
    public List<String> LT = new ArrayList<>();
    public Context fragment;
    private DatabaseReference mDataBase;
    public SavePartsID savePartsID = new SavePartsID();
    public String user = FirebaseAuth.getInstance().getUid();

    public AdapterMyParts(List<String> list, Context fragment) {
        numberItems = list.size();
        LT = list;
        this.fragment = fragment;
    }


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public AdapterMyParts.RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.parts;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        AdapterMyParts.RecyclerviewHolder viewHolder = new AdapterMyParts.RecyclerviewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMyParts.RecyclerviewHolder holder, int viewHolderCount) {
        holder.bind(viewHolderCount);
    }


    @Override
    public int getItemCount() {
        return numberItems;
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {

        TextView ItemList;

        public RecyclerviewHolder(@NonNull View itemView) {
            super(itemView);

            ItemList = itemView.findViewById(R.id.title_parts);
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    savePartsID.setTitle((String) ItemList.getText());
                    SaveID saveID = new SaveID();
                    mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts")
                            .child(savePartsID.getTitle()).push();
                    mDataBase.setValue(saveID);
                }
            });
        }

        void bind(int ListIndex) {
            ItemList.setText(LT.get(ListIndex));
        }
    }
}
