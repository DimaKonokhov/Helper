package com.example.helper.ui.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.SaveID;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerviewHolder> {

    private final int numberItems;
    public String[] LT;
    public int[] DLT;
    private Context fragment;

    public RecyclerAdapter(String[] list, int[] DrawableList, Context fragment) {
        numberItems = list.length;
        LT = list;
        DLT = DrawableList;
        this.fragment = fragment;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_item;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        RecyclerviewHolder viewHolder = new RecyclerviewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewHolder holder, int viewHolderCount) {
        holder.bind(viewHolderCount);
    }


    @Override
    public int getItemCount() {
        return numberItems;
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {
        TextView ItemList;
        ImageView image;

        public RecyclerviewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            ItemList = itemView.findViewById(R.id.Recycler);
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SaveID saveID = new SaveID();
                    saveID.setName((String) ItemList.getText());
                    Navigation.findNavController(view).navigate(R.id.listItemFragment);
                }
            });

        }

        void bind(int ListIndex) {
            ItemList.setText(LT[ListIndex]);
            image.setBackgroundResource(DLT[ListIndex]);
        }

    }
}

