package com.example.helper.ui.auth;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.databinding.FragmentPartsBinding;
import com.example.helper.ui.DialogNewMyParts;
import com.example.helper.ui.List.AdapterMyParts;
import com.example.helper.ui.List.AdapterParts;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SavePartsID;
import com.example.helper.viewmodels.PartsViewModel;
import com.google.firebase.auth.FirebaseAuth;

public class PartsFragment extends Fragment {
    public PartsViewModel viewModel;
    public FragmentPartsBinding binding;
    public AdapterParts adapter;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPartsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        SavePartsID savePartsID = new SavePartsID();
        String user = FirebaseAuth.getInstance().getUid();
        viewModel = new ViewModelProvider(this).get(PartsViewModel.class);
        viewModel.Data(user);

        LiveData<Boolean> data = viewModel.data;
        data.observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                binding.recyclerParts.setHasFixedSize(true);
                adapter = new AdapterParts(savePartsID.getLS(), getActivity());
                binding.recyclerParts.setAdapter(adapter);
            }
        });

        binding.newMyParts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogNewMyParts newMyParts = new DialogNewMyParts(getActivity());
                newMyParts.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                newMyParts.show();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
        super.onStart();
    }
    @Override
    public void onDestroy() {
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        super.onDestroy();
    }
}