package com.example.helper.ui;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.List.AdapterMyParts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DialogAddParts extends Dialog {
    public Activity activity;
    public AdapterMyParts adapter;
    public RecyclerView recycler;
    public TextView newMyParts;
    public DatabaseReference mDataBase;
    public int i = 0;

    public DialogAddParts(Activity a) {
        super(a);
        activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.y = 20;

        SavePartsID savePartsID = new SavePartsID();
        recycler = findViewById(R.id.recycler_parts);
        newMyParts = findViewById(R.id.newParts);

        recycler.setHasFixedSize(true);
        adapter = new AdapterMyParts(savePartsID.getLS(), activity);
        recycler.setAdapter(adapter);

        newMyParts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogNewMyParts newMyParts = new DialogNewMyParts(activity);
                newMyParts.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                newMyParts.show();
            }
        });

        String user = FirebaseAuth.getInstance().getUid();
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (i == 1) dismiss();
                i++;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}

