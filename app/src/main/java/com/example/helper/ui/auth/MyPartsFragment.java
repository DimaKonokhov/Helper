package com.example.helper.ui.auth;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.helper.R;
import com.example.helper.databinding.FragmentMyPartsBinding;
import com.example.helper.ui.DialogDeleteMyParts;
import com.example.helper.ui.List.AdapterLikeParts;
import com.example.helper.ui.List.AdapterParts;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SavePartsID;
import com.example.helper.viewmodels.MyPartsViewModel;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class MyPartsFragment extends Fragment {

    private MyPartsViewModel mViewModel;
    private FragmentMyPartsBinding binding;
    private AdapterLikeParts adapter;
    private String[] list = {};
    private String[] DrawableList = {};

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentMyPartsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        String user = FirebaseAuth.getInstance().getUid();
        SavePartsID savePartsID = new SavePartsID();
        mViewModel = new ViewModelProvider(this).get(MyPartsViewModel.class);

        mViewModel.Data(user, savePartsID.getTitle());

        LiveData<List<String>> data = mViewModel.data;
        data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                DrawableList = strings.toArray(new String[strings.size()]);
            }
        });

        LiveData<List<String>> Data = mViewModel.Data;
        Data.observe(getActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> s) {
                list = s.toArray(new String[s.size()]);
                binding.recyclerParts.setHasFixedSize(true);
                adapter = new AdapterLikeParts(list, DrawableList, getActivity());
                binding.recyclerParts.setAdapter(adapter);
            }
        });

        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogDeleteMyParts dialog = new DialogDeleteMyParts(getActivity(), user, savePartsID.getTitle());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                dialog.show();
            }
        });

        binding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
                ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
                Navigation.findNavController(view).navigate(R.id.action_myPartsFragment_to_homeFragment);
            }
        });
        return view;
    }
}