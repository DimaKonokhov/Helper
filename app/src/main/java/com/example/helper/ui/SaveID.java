package com.example.helper.ui;

public class SaveID {
    private static String name;
    private static Integer id;
    private static String img;
    private static String title;

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getID() {
        return id;
    }
    public void setImg(String img) {
        this.img = img;
    }
    public String getImg() {
        return img;
    }
}
