package com.example.helper.ui.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helper.R;
import com.example.helper.ui.SaveID;
import com.squareup.picasso.Picasso;


public class AdapterLikeParts extends RecyclerView.Adapter<AdapterLikeParts.RecyclerviewHolder> {
    private final int numberItems;
    public String[] LT;
    public String[] LI;
    private Context fragment;

    public AdapterLikeParts(String[] list, String[] listItem, Context fragment) {
        numberItems = list.length;
        LT = list;
        LI = listItem;
        this.fragment = fragment;
    }


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.parts_like;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        RecyclerviewHolder viewHolder = new RecyclerviewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewHolder holder, int viewHolderCount) {
        holder.bind(viewHolderCount);
    }


    @Override
    public int getItemCount() {
        return numberItems;
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {
        TextView ItemList;
        ImageView Image;

        public RecyclerviewHolder(@NonNull View itemView) {
            super(itemView);
            SaveID saveID = new SaveID();
            ItemList = itemView.findViewById(R.id.title);
            Image = itemView.findViewById(R.id.parts_like);
            Image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Navigation.findNavController(view).getCurrentDestination().getId() == R.id.favoritesPartsFragment) {
                        saveID.setTitle((String) ItemList.getText());
                        Navigation.findNavController(view).navigate(R.id.favoritesPartsStateFragment);
                    } else {
                        saveID.setTitle((String) ItemList.getText());
                        //Navigation.findNavController(view).navigate(R.id.);
                    }
                }
            });
            ItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Navigation.findNavController(view).getCurrentDestination().getId() == R.id.favoritesPartsFragment) {
                        saveID.setTitle((String) ItemList.getText());
                        Navigation.findNavController(view).navigate(R.id.favoritesPartsStateFragment);
                    }else {
                        saveID.setTitle((String) ItemList.getText());
                        Navigation.findNavController(view).navigate(R.id.myPartsStateFragment);
                    }
                }
            });

        }

        void bind(int ListIndex) {
            ItemList.setText(LT[ListIndex]);
            Picasso.get().load(LI[ListIndex]).into(Image);
        }
    }
}
