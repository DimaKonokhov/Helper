package com.example.helper.ui.auth;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.helper.R;
import com.example.helper.databinding.FragmentArticlesInfoBinding;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;
import com.example.helper.viewmodels.ArticlesInfoViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ArticlesInfoFragment extends Fragment {
    public FragmentArticlesInfoBinding binding;
    public ArticlesInfoViewModel viewModel;
    public DatabaseReference mDataBase;
    public FirebaseAuth mAuth;
    public String user;
    public int i, key;
    public boolean b;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentArticlesInfoBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        viewModel = new ViewModelProvider(this).get(ArticlesInfoViewModel.class);
        SaveID saveID = new SaveID();

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            user = FirebaseAuth.getInstance().getCurrentUser().getUid();
            ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
            ((MainActivity) requireActivity()).like.setVisibility(View.VISIBLE);
        }

        if (user != null){
            viewModel.DataBool(saveID.getName(), user);

            mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("article");
            mDataBase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    i = 0;
                    key = 0;
                    for (DataSnapshot ds: snapshot.getChildren()){
                        key = Integer.parseInt(ds.getKey());
                        String ids = ds.getValue(String.class);
                        if (ids.equals(saveID.getName())){
                            i = Integer.parseInt(ds.getKey());
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });
        }

        viewModel.mData(saveID.getName());

        LiveData<Boolean> Data = viewModel.Data;
        Data.observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean bool) {
                b =bool;
            }
        });

        LiveData<String> data = viewModel.data;
        data.observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (b == true){
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                }
                binding.info.setText(s);
            }
        });

        ((MainActivity) requireActivity()).like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!b){
                    b = true;
                    viewModel.Like(saveID.getName(), key, user);
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector_like);
                }else {
                    b = false;
                    viewModel.removeLike(i, user);
                    ((MainActivity) requireActivity()).like.setBackgroundResource(R.drawable.vector);
                }
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).navView.setVisibility(View.GONE);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) requireActivity()).toolbar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).navView.setVisibility(View.VISIBLE);
        ((MainActivity) requireActivity()).like.setVisibility(View.GONE);
    }
}