package com.example.helper.ui.auth;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.helper.R;
import com.example.helper.databinding.FragmentAuthBinding;
import com.example.helper.ui.MainActivity;
import com.example.helper.viewmodels.AuthViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AuthFragment extends Fragment {
    private FragmentAuthBinding binding;
    private AuthViewModel mViewModel;
    private FirebaseAuth mAuth;
    private View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAuthBinding.inflate(inflater, container, false);
        view = binding.getRoot();

        mViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        binding.enter.setEnabled(false);


        EditText[] edList = {binding.login, binding.password};
        CustomTextWatcher textWatcher = new CustomTextWatcher(edList, binding.enter);
        for (EditText editText : edList) editText.addTextChangedListener(textWatcher);

        LiveData<Boolean> data = mViewModel.data;
        data.observe(requireActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean b) {
                if (b) {
                    ((MainActivity) requireActivity()).onSupportNavigateUp();
                    binding.error.setVisibility(View.INVISIBLE);
                } else {
                    binding.error.setVisibility(View.VISIBLE);
                }
            }
        });

        binding.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.password.getText().toString().length() >= 6) {
                    binding.error.setText("Неправильный логин или пароль");
                    mViewModel.senData(binding.login.getText().toString(), binding.password.getText().toString());
                } else {
                    binding.error.setText("Пароль содержит меньше 6 символов");
                    mViewModel.senData(binding.login.getText().toString(), binding.password.getText().toString());
                }
            }
        });

        binding.registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.registrFragment);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Navigation.findNavController(view).navigate(R.id.profileFragment);
        }
    }

    public static class CustomTextWatcher implements TextWatcher {
        View v;
        EditText[] edList;
        public CustomTextWatcher(EditText[] edList, TextView v) {
            this.v = v;
            this.edList = edList;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            for (EditText editText : edList) {
                if (editText.getText().toString().trim().length() == 0) {
                    v.setEnabled(false);
                    break;
                } else v.setEnabled(true);
            }
        }
    }
}