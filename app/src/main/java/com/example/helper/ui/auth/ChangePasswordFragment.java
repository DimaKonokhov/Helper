package com.example.helper.ui.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.helper.databinding.FragmentChangePasswordBinding;
import com.example.helper.viewmodels.ChangePasswordViewModel;

public class ChangePasswordFragment extends Fragment {
    public FragmentChangePasswordBinding binding;
    private ChangePasswordViewModel model;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        model = new ViewModelProvider(this).get(ChangePasswordViewModel.class);

        LiveData<String> data = model.data;
        data.observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
            }
        });

        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.passwordNew.getText().toString().equals(binding.passwordPovto.getText().toString())) {
                    if (binding.passwordNew.getText().toString().length() >= 6) {
                        model.senData(binding.passwordNew.getText().toString());
                        binding.passwordNew.setText("");
                        binding.passwordPovto.setText("");
                    } else {
                        Toast.makeText(getActivity(), "Пароль содержит меньше 6 символов", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Вы не подтвердили пароль!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}