package com.example.helper.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;

import com.example.helper.R;
import com.example.helper.ui.auth.AuthFragment;
import com.example.helper.ui.auth.MyPartsFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DialogNewMyParts extends Dialog {
    private EditText title;
    private TextView exit;
    private TextView confirm;
    private DatabaseReference mDataBase;
    private Context context;

    public DialogNewMyParts(@NonNull Context context) {
        super(context);
        this.context = context;;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_my_parts);

        String user = FirebaseAuth.getInstance().getUid();
        SavePartsID savePartsID = new SavePartsID();
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts");

        title = findViewById(R.id.new_title);
        exit = findViewById(R.id.exit);
        confirm = findViewById(R.id.confirm);

        confirm.setEnabled(false);
        EditText[] edList = {title};
        AuthFragment.CustomTextWatcher textWatcher = new AuthFragment.CustomTextWatcher(edList, confirm);
        for (EditText editText : edList) editText.addTextChangedListener(textWatcher);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = 0;
                for (String ls: savePartsID.getLS()){
                    if (ls.equals(title)){
                        i++;
                        break;
                    }
                }
                if (i == 0){
                    mDataBase.child(String.valueOf(title.getText())).setValue(0);
                    dismiss();
                }
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
