package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SavePartsID;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PartsViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<Boolean> data = _data;
    private DatabaseReference mDataBase;
    public List<String> NameParts = new ArrayList<>();
    public SavePartsID savePartsID = new SavePartsID();
    public void Data(String user){
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                NameParts.clear();
                for (DataSnapshot ds: snapshot.getChildren()){
                    String title = ds.getKey();
                    NameParts.add(title);
                }
                savePartsID.setLS(NameParts);
                _data.setValue(true);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

}