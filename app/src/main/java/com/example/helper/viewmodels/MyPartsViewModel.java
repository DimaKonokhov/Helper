package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.helper.ui.SaveID;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MyPartsViewModel extends ViewModel {
    public DatabaseReference mDataBase;
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<List<String>> data = _data;
    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<List<String>> Data = _Data;
    private List<String> titles = new ArrayList<>();
    private List<String> image = new ArrayList<>();

    public void Data(String user, String title) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts").child(title);
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                image.clear(); titles.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    String title = ds.child("title").getValue(String.class);
                    String img = ds.child("img").getValue(String.class);
                    titles.add(title);
                    image.add(img);
                }
                _data.setValue(image);
                _Data.setValue(titles);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}