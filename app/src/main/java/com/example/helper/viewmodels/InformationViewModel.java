package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.helper.R;
import com.example.helper.ui.MainActivity;
import com.example.helper.ui.SaveID;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class InformationViewModel extends ViewModel {
    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<String> Data = _Data;
    private MutableLiveData _data = new MutableLiveData(false);
    public LiveData<Boolean> data = _data;
    private DatabaseReference mDataBase;
    private int i;
    public void infoData(int i, String s){
        mDataBase = FirebaseDatabase.getInstance().getReference(s).child(""+i+"");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String img = snapshot.child("img").getValue(String.class);
                SaveID saveID = new SaveID();
                saveID.setImg(img);
                _Data.setValue(img);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void DataBool(String title, String user) {
        i = 0;
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds : snapshot.getChildren()) {
                    _data.setValue(false);
                    String titles = ds.child("title").getValue(String.class);
                    if (title.equals(titles) && i == 0) {
                        i++;
                        _data.setValue(true);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void Like(int key, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        SaveID saveID = new SaveID();
        key++;
        mDataBase.child("" + key + "").setValue(saveID);
    }

    public void removeLike(int id, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.child("" + id + "").removeValue();
    }
}