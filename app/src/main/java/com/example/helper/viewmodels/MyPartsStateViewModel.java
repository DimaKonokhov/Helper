package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.helper.ui.SaveID;
import com.example.helper.ui.SavePartsID;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MyPartsStateViewModel extends ViewModel {
    private DatabaseReference mDataBase;
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<String> data = _data;

    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<Boolean> Data = _Data;

    public SaveID saveID = new SaveID();
    public SavePartsID savePartsID = new SavePartsID();
    public int i;
    public void mData(String user, String title, String MyTitles){
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts").child(MyTitles);
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()){
                    String titles = ds.child("title").getValue(String.class);
                    String key = ds.getKey();
                    if (title.equals(titles) && i == 0){
                        String name = ds.child("name").getValue(String.class);
                        Integer id = ds.child("id").getValue(Integer.class);
                        String img = ds.child("img").getValue(String.class);
                        saveID.setName(name);
                        saveID.setId(id);
                        saveID.setImg(img);
                        savePartsID.setId(key);
                        _data.setValue(saveID.getImg());
                        i++;
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void BoolLike(String user, String title){
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()){
                    String titles = ds.child("title").getValue(String.class);
                    if (title.equals(titles)){
                        _Data.setValue(true);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void Like(int key, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        SaveID saveID = new SaveID();
        key++;
        mDataBase.child("" + key + "").setValue(saveID);
    }

    public void removeLike(int id, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.child("" + id + "").removeValue();
    }

    public void removeMyParts(String key, String user, String MyTitles){
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("parts").child(MyTitles);
        mDataBase.child(key).removeValue();
    }
}