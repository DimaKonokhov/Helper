package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FavoritesArticleViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<List<String>> data = _data;
    public List<String> list = new ArrayList<>();
    private DatabaseReference mDataBase;
    public void Data(){
        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("article");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();
                for (DataSnapshot ds: snapshot.getChildren()){
                    list.add(ds.getValue(String.class));
                }
                _data.setValue(list);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }
}