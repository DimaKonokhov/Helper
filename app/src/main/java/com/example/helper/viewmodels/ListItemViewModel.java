package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListItemViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<List<String>> data = _data;
    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<List<String>> Data = _Data;
    private DatabaseReference mDataBase;
    public void Data(String name) {
        List<String> LS = new ArrayList<>();
        List<String> LSD = new ArrayList<>();
        mDataBase = FirebaseDatabase.getInstance().getReference(name);
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (LS.size() > 0) LS.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    String title = ds.child("title").getValue(String.class);
                    String img = ds.child("img").getValue(String.class);
                    LSD.add(img);
                    LS.add(title);
                }
                _Data.setValue(LSD);
                _data.setValue(LS);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }
}