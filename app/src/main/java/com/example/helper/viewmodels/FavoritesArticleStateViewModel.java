package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FavoritesArticleStateViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<String> data = _data;
    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<Boolean> Data = _Data;
    private DatabaseReference mDataBase;
    private DatabaseReference DataBase;
    public void mData(String titles) {
        DataBase = FirebaseDatabase.getInstance().getReference("Information");
        mDataBase = FirebaseDatabase.getInstance().getReference("header");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()){
                    String title = ds.getValue(String.class);
                    if (title.equals(titles)){
                        int id = Integer.parseInt(ds.getKey());
                        DataBase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                String info = snapshot.child(""+id+"").getValue(String.class);
                                _data.setValue(info);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void DataBool(String key, String user) {
        DataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("article");
        DataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds : snapshot.getChildren()) {
                    String ids = ds.getValue(String.class);
                    if ((key.equals(ids))) {
                        _Data.setValue(true);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    public void Like(String id, int key, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("article");
        key++;
        mDataBase.child("" + key + "").setValue(id);
    }

    public void removeLike(int id, String user) {
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("article");
        mDataBase.child("" + id + "").removeValue();
    }
}