package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<String> data = _data;
    public FirebaseUser user;
    public FirebaseAuth mAuth;
    public void senData(String password) {
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        user.updatePassword(password).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    _data.setValue("Вы поменяли пароль");
                }else {
                    _data.setValue("Ошибка!!!");
                }
            }
        });
    }
}