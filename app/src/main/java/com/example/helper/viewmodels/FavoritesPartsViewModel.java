package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FavoritesPartsViewModel extends ViewModel {
    private DatabaseReference mDataBase;
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<List<String>> data = _data;
    private MutableLiveData _Data = new MutableLiveData();
    public LiveData<List<String>> Data = _Data;
    private List<String> titles = new ArrayList<>();
    private List<String> image = new ArrayList<>();
    public void mData(String user){
        mDataBase = FirebaseDatabase.getInstance().getReference("users").child(user).child("like").child("parts");
        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                titles.clear(); image.clear();
                for (DataSnapshot ds: snapshot.getChildren()) {
                    String img = ds.child("img").getValue(String.class);
                    String title = ds.child("title").getValue(String.class);
                    image.add(img);
                    titles.add(title);
                }
                _data.setValue(image);
                _Data.setValue(titles);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }
}