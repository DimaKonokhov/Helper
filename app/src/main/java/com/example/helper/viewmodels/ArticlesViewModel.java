package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ArticlesViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData();
    public LiveData<List<String>> liveData = _data;
    private List<String> list = new ArrayList<>();
    public void Data(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("header");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(list.size() > 0)list.clear();
                for(DataSnapshot DS:snapshot.getChildren()){
                    String value = DS.getValue(String.class);
                    list.add(value);
                }
                _data.setValue(list);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}