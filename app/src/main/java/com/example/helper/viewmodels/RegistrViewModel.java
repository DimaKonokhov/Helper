package com.example.helper.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.helper.ui.SaveProfile;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrViewModel extends ViewModel {
    private MutableLiveData _data = new MutableLiveData(false);
    public LiveData<Boolean> data = _data;
    private FirebaseAuth mAuth;
    private DatabaseReference mDataBase;
    public void sentData(String login, String password, String name) {
        mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(login, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    mDataBase = FirebaseDatabase.getInstance().getReference("users");
                    SaveProfile NewProfile = new SaveProfile(login, name);
                    mDataBase.child(user).setValue(NewProfile);

                    mAuth.signInWithEmailAndPassword(login, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            _data.setValue(true);
                        }
                    });
                }
            }
        });
    }
}